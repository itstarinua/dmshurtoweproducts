<?php
class DmshurtoweproductsShowModuleFrontController extends ModuleFrontController
{
    public function displayAjax()
    {
        header('Content-type: application/json');
        header('Location: '.$this->context->link->getPageLink('404'));
		exit;
    }
    
    protected function disableColumns()
    {
        $this->display_column_left = false;
        $this->display_column_right = false;
        $this->display_footer = false;
        $this->display_header = false;
    }
    public static $image_base_url = false;
    protected static function get_image_url( $attribut )
    {        
        if( isset( $attribut['is_color_group']) 
            && (int)$attribut['is_color_group'] 
            && ( file_exists(_PS_COL_IMG_DIR_.(int)$attribut['id_attribute'].'.jpg') ) 
        ) {
            if( !(self::$image_base_url) )
            {
                self::$image_base_url = str_replace(_PS_ROOT_DIR_, '', _PS_COL_IMG_DIR_);
            }   
            return self::$image_base_url.(int)$attribut['id_attribute'].'.jpg';
        }
        return false;
    }
    public function initContent()
    {
        $this->disableColumns();
        $calc_tax = (bool)Product::getTaxCalculationMethod( (int)$this->context->cookie->id_customer );
        parent::initContent();
        $products = Product::getProducts( $this->context->language->id, 0, 100000000, 'id_product', 'DESC', false, $only_active = true);
        foreach( $products as &$products_data)
        {
            $product = new Product( (int)$products_data['id_product'], false, $this->context->language->id);
            $attributes_groups = $product->getAttributesGroups( $this->context->language->id  );
            $groups = array();
            $products_data['price'] = $product->getPrice( !$calc_tax );
            $products_data['show_price'] = isset($this->context->customer->id) ? $product->show_price : false;
            foreach ($attributes_groups as $attribut_key => $attribut) 
            {
                $id_group = (int)$attribut['id_attribute_group'];
                if( !isset( $groups[ $id_group ] ) )
                {
                    $groups[ $id_group ] = array(
                        'id' => $id_group,
                        'group_name' => $attribut['group_name'],
                        'group_name_public' => $attribut['public_group_name'],
                        'group_type' => $attribut['group_type'],
                        'default' => false,
                        'attributes' => array(),
                        'is_color_group' => $attribut['is_color_group'],
                    );
                    if( isset( $attribut['probnik_name'] ) )
                    {
                        $groups[ $id_group ]['probnik_name'] = $attribut['probnik_name'];
                    }
                }
                if( !isset( $groups[ $id_group ]['attributes'][ $attribut['id_attribute']  ] ) )
                {                    
                    $groups[ $id_group ]['attributes'][ $attribut['id_attribute']  ] = array(
                        'id' => $attribut['id_attribute'],
                        'name' => $attribut['attribute_name'],
                        'attribute_color' => $attribut['attribute_color'],
                        'is_color_group' => $attribut['is_color_group'],
                        'file' => self::get_image_url($attribut),
                    );
                    
                }
            }
            $products_data[ 'dms_groups'] = $groups;
            $products_data[ 'dms_images'] = Image::getImages( $this->context->language->id, (int)$products_data['id_product']  );
            //tomsky_die($products_data[ 'dms_images']);
            $image = Image::getCover( (int)$products_data['id_product'] );
            foreach( $products_data[ 'dms_images'] as &$img )
            {
                $img[ 'image_link'] = $this->img_exist( $img['id_image'] );
                if( $img['legend'] == 'WX' || $img['legend'] == 'BWX' ) 
                {
                    $products_data[ 'image_link'] = $img[ 'image_link'];
                }
            }
            $products_data['features'] = $product->getFeatures();
            if( count( $products_data['features'] ) )
            {
                foreach( $products_data['features'] as &$feature)
                {
                    $feature['name'] = $this->getFeatureName( $feature['id_feature'] );      
                    $feature['value'] = $this->getFeatureValue( $feature['id_feature'], $feature['id_feature_value'] );
                }
            }
        }
        $this->context->smarty->assign( 
            array( 
                'all_products' => $products,
            ) 
        );        
       $this->setTemplate('showlist.tpl');                   
    }
    protected $feature_values_with_lang = array();
    protected function getFeatureValue( $id_feature = 0 , $id_feature_value = 0 ){
        if( !isset( $feature_values_with_lang[ $id_feature ] ) )
        {
            $feature_values_with_lang[ $id_feature ] = FeatureValueCore::getFeatureValuesWithLang($this->context->language->id, $id_feature,$this->context );    
        }
        if( count( $feature_values_with_lang[ $id_feature ] ) ) 
        {
            foreach($feature_values_with_lang[ $id_feature ] as $feature )
            {
                if( (int)$feature['id_feature_value'] == (int)$id_feature_value )
                {
                    return $feature['value'];
                }
            }
        }
        return '';
    }
    protected $feature_names = array();
    protected function getFeatureName( $id_feature = 0 ){
        if( !isset( $feature_names[ $id_feature ] ) )
        {
            $feature_names[ $id_feature ] = FeatureCore::getFeature($this->context->language->id, $id_feature);
        }        
        return isset($feature_names[ $id_feature ]['name'] ) ? $feature_names[ $id_feature ]['name'] : '';
    }
    public function img_exist( $id_image = 0 )
    {
        for( $i=0;$i<10;$i++)
        {
            $image_link = '';
            switch($i)
            {
                case 0:
                {
                    $image_link = _THEME_PROD_DIR_.Image::getImgFolderStatic( $id_image ).$id_image.'-WX.jpg';
                    break;
                }
                case 1:
                {
                    $image_link = _THEME_PROD_DIR_.Image::getImgFolderStatic( $id_image ).$id_image.'-BWX.jpg';
                    break;
                }
                case 2:
                {
                    $image_link = _THEME_PROD_DIR_.Image::getImgFolderStatic( $id_image ).$id_image.'-small_default.jpg';
                    break;
                }
                case 3:
                {
                    $image_link = __PS_BASE_URI__.Image::getImgFolderStatic( $id_image ).$id_image.'-WX.jpg';
                    break;
                }
                case 4:
                {
                    $image_link = __PS_BASE_URI__.Image::getImgFolderStatic( $id_image ).$id_image.'-BWX.jpg';
                    break;
                }
                case 5:
                {
                    $image_link = __PS_BASE_URI__.Image::getImgFolderStatic( $id_image ).$id_image.'-small_default.jpg';
                    break;
                }
                case 6:
                {
                    $products_data[ 'image_link'] = _THEME_PROD_DIR_.Image::getImgFolderStatic( $image['id_image'] ).$image['id_image'].'.jpg';
                    break;
                }
                case 7:
                {
                    $products_data[ 'image_link'] = __PS_BASE_URI__.Image::getImgFolderStatic( $image['id_image'] ).$image['id_image'].'.jpg';
                    break;
                }
            }
            if( file_exists(  _PS_ROOT_DIR_.$image_link ) ) 
            {
                return $image_link;
            }
        }
        return false;
    }
}

?>