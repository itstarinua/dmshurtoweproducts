{if empty($display_header)}
<!DOCTYPE html>
<html class="version-00">
    <head>
        <style>
            table,
            table td,
            table th{
                border: 1px solid black;
            }
            table th{
                background: #ececec;
            }
            .group-type-color .color_pick{
               display: inline-block;
               min-width:20px;
               min-height:20px;
               border: 1px solid #ececec;
               vertical-align: middle;
            }
            .group-type-color .color_pick img {
                vertical-align: bottom;
            }
            .invert-color{
                -webkit-filter: invert(100%);
                filter: invert(100%);
            }
            .color_photo {
                display: inline-block;
                border: 1px solid #ececec;
                width:60px;
                height:100px;
            }
            .color_photo p{
                text-align: center;
                font-weight: bold;
            }
            .images {
                text-align: center;
                max-width: 33.3333333%;
            }
            .body {
                font-weight: 100;
                font-family: "Lato", sans-serif;
	    }
        </style>
    </head>
<body>
{/if}
<table>

    <tbody>
{if $all_products}
    {foreach from=$all_products item=product}
        <tr class="product-id-{$product['id_product']} main">
		            <td class="name">
                <b>{$product['name']}</b>
            </td>    
            <td class="price">
               	{if $product['show_price'] }
                    {displayPrice price=$product['price']}
                {else}
                    <a class="exclusive btn btn-default" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow">{l s='Login to view prices'}</a>
                {/if}
            </td>    

        </tr>
        <tr class="product-id-{$product['id_product']} photos">
            <td colspan="2">                
                {foreach from=$product['dms_images'] item=image}
                    {if ( ( $image['legend'] !== 'WX' ) && ( $image['legend'] !== 'BWX' ) )}<img src="{$image['image_link']}" style="width:60px;float:left"/>{/if}
                {/foreach}
            </td>    
        </tr>
        <tr class="product-id-{$product['id_product']} description">
            <td colspan="2">                     
                {if !empty($product['description_short'])}<div class="short-description">{$product['description_short']}</div>{/if}
                {if count($product['dms_groups'])}
                    {foreach from=$product['dms_groups'] item=groups}
                        {if count($groups['attributes'])}
                            <div class="group group-id-{$groups['id']} group-type-{$groups['group_type']}"><b><span class="group-title">{if $groups['group_type'] == 'color'} {else}{$groups['group_name_public'] }:{/if}</span></b> 
                            {if $groups['group_type'] == 'color'}
                                {assign var="default_colorpicker" value=""}

                            {else}
                                {foreach from=$groups['attributes'] item=attr key=key}
                                    <span class="attr attr-id-{$key}">{$attr['name']}</span>,
                                {/foreach}
                            {/if}
                            </div>
                        {/if}
                    {/foreach}
                {/if}
            </td>    
        </tr>
        {if !empty( $product['image_link'] )}
        <tr class="product-id-{$product['id_product']} images">
            <td colspan="2">                
                {if count($product['dms_groups'])}
                    {foreach from=$product['dms_groups'] item=groups}
                        {if count($groups['attributes'])}
                            {if $groups['group_type'] == 'color'}
                                {assign var="default_colorpicker" value=""}
                                {foreach from=$groups['attributes'] key=key item=attr}
								<div style="float:left;width:90px;">
                                    <div id="color_{$key|intval}" class="color_photo" 
                                        {if $attr['file']}                                             
                                            style="background:url({$attr['file']}) top left no-repeat;background-size:60px;"
                                        {elseif isset($attr['attribute_color']) && !empty($attr['attribute_color']) }
                                            style="background:{$attr['attribute_color']|escape:'html':'UTF-8'};"
                                        {/if}>
                                        <img src="{$product['image_link']}" style="width:60px;"/>
                                    </div>
									<div style="font-size:11px;">{$attr['name']}</div>
									</div>
                                {/foreach}
                            {/if}
                            
                        {/if}
                    {/foreach}
                {else}
                    <img src="{$product['image_link']}"/>
                {/if}
            </td>    
        </tr>
        {/if}
    {/foreach}
{/if}
</tbody>
{if empty($display_footer)}
</table>
</body>
</html>
{/if}