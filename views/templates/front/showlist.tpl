{if empty($display_header)}
<!DOCTYPE html>
<html class="version-01">
    <head>
        <style>
            table,
            table td,
            table th{
                border: 0px solid black;
                width:100%;
            }
            table th{
                background: #ececec;
            }
            .group-type-color .color_pick{
               display: inline-block;
               min-width:20px;
               min-height:20px;
               border: 1px solid #ececec;
               vertical-align: middle;
            }
            .group-type-color .color_pick img {
                vertical-align: bottom;
            }
            .invert-color{
                -webkit-filter: invert(100%);
                filter: invert(100%);
            }
            .color_photo {
                display: inline-block;
            }
            .color_photo p{
                text-align: center;
                font-weight: bold;
            }
            .images {
                text-align: center;
                max-width: 33.3333333%;
            }
            .body {
                margin:auto;
                font-family: "Lato", sans-serif;
                text-transform: uppercase;
	    }
            .produkt {
                height:1100px;
                width:100%;
                padding:10px;
            }
            .product-features-list {
                display: inline-block;
                border: 1px solid red;
                margin-left: 10px;
                padding: 6px;
            }
            .probnik_name {
                margin-left: 6px;
                display: initial;
            }
        </style>
    </head>
<body class="body">
{/if}


    <tbody>
{if $all_products}
    {foreach from=$all_products item=product}
        
<div class="produkt">
<table>
        <tr class="product-id-{$product['id_product']} main">
		            <td class="name" style="border: 1px solid #060606;">
               	{if $product['show_price'] }
                    <div style="float:left;background:#000000;padding:6px;color:#ffffff;">{displayPrice price=$product['price']}</div>
                {else}
                    <div style="float:left;background:#000000;padding:6px;color:#ffffff;"><a class="exclusive btn btn-default" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow">{l s='Login to view prices'}</a></div>
                {/if}
                <div style="float:left;margin-left:6px;padding:6px;">{$product['name']}</div>
            </td>    
            <td class="price">

            </td>    

        </tr>
        <tr class="product-id-{$product['id_product']} photos">
            <td colspan="2">                
                {foreach from=$product['dms_images'] item=image}
                    {if ( ( $image['legend'] !== 'WX' ) && ( $image['legend'] !== 'BWX' ) )}<img src="{$image['image_link']}" style="width:175px;float:left"/>{/if}
                {/foreach}
		
            </td>    
        </tr>
        <tr class="product-id-{$product['id_product']} description">
            <td colspan="2"><br />  
                            
                {if !empty($product['description_short'])}<div class="short-description" style="display:none;">{$product['description_short']}</div>{/if}
                {if count($product['dms_groups'])}
                    {foreach from=$product['dms_groups'] item=groups}
                        {if count($groups['attributes'])}
                            <div class="group group-id-{$groups['id']} group-type-{$groups['group_type']}" {if $groups['group_type'] == 'color'}style="border:1px solid #000000;height:60px;width:98%;"{/if}><b><span style="padding:6px;" class="group-title">{if $groups['group_type'] == 'color'}{else}{$groups['group_name_public'] }:{/if} </span></b> 
                            
                            {if $groups['group_type'] == 'color'}
                                {assign var="default_colorpicker" value=""}
                                {capture name="probnik_name"}
                                    {if $groups['probnik_name'] == '0' || $groups['probnik_name'] == ''}
                                    {else}
                                        , <span class="probnik_name" style="float:right;" >
                                            {l s='Sampler No.: '}
                                            <span style="">
                                                <span style="">
                                                    {$groups['probnik_name']}
                                                </span>
                                            </span>
                                        </span>
                                    {/if}
                                {/capture}
                            {else}
                                {foreach from=$groups['attributes'] item=attr key=key}
                                    <span class="attr attr-id-{$key}" style="background:#000000;color:#ffffff;padding:6px;">{$attr['name']}</span>
                                {/foreach}
                            {/if}
                        {/if}
                    {/foreach}
                {/if}
                {if isset($product['features'])} 
                    <div class="product-features-list">
                    {foreach from=$product['features'] item=feature}
                        {if isset($feature.value)} 
                            <span class="product-feature">{$feature.name|escape:'html':'UTF-8'}: {$feature.value|escape:'html':'UTF-8'}</span>                            
                        {/if}
                    {/foreach}
                    {if $smarty.capture.probnik_name}{$smarty.capture.probnik_name}{/if}
                    </div>
                {/if}
            </td>    
        </tr>
        {if !empty( $product['image_link'] )}
        <tr class="product-id-{$product['id_product']} images">
            <td colspan="2">  <br />            
                {if count($product['dms_groups'])}
                    {foreach from=$product['dms_groups'] item=groups}
                        {if count($groups['attributes'])}
                            {if $groups['group_type'] == 'color'}
                                {assign var="default_colorpicker" value=""}
                                {foreach from=$groups['attributes'] key=key item=attr}
								<div style="float:left;width:95px;margin-bottom:8px;">
                                    <div id="color_{$key|intval}" class="color_photo"
                                        {if $attr['file']}                                             
                                            style="background:url({$attr['file']}) top left no-repeat;background-size:60px;width:60px;height:100px;"
                                        {elseif isset($attr['attribute_color']) && !empty($attr['attribute_color']) }
                                            style="background:{$attr['attribute_color']|escape:'html':'UTF-8'};height:100px;"
                                        {/if}>
                                        <img src="{$product['image_link']}" style="width:60px;"/>
                                    </div>
									<div style="font-size:10px;">{$attr['name']}</div>
									</div>
                                {/foreach}
                            {/if}
                            
                        {/if}
                    {/foreach}
                {else}
                    <img src="{$product['image_link']}"/>
                {/if}
            </td>    
        </tr>
        {/if}
</table>
</div>
    {/foreach}
{/if}
</tbody>
{if empty($display_footer)}

</body>
</html>
{/if}